#!/bin/bash

# Ansi color code variables
# shellcheck disable=SC2034
red="\e[0;91m"
# shellcheck disable=SC2034
blue="\e[0;94m"
# shellcheck disable=SC2034
green="\e[0;92m"
# shellcheck disable=SC2034
white="\e[0;97m"
# shellcheck disable=SC2034
bold="\e[1m"
# shellcheck disable=SC2034
uline="\e[4m"
# shellcheck disable=SC2034
reset="\e[0m"

if [[ ! -v $KDELAY ]]; then
		KDELAY="10"
fi

function _flatpakPrep() {
		echo -e "${blue}Setting up Flatpak permissions...${reset}"
		flatpak override --filesystem=~/.reguilded --user
		echo -e "${green}Permissions set!${reset}"
		echo -e "${blue}Installing Guilded...${reset}"
		flatpak install gg.guilded.Guilded --noninteractive
}

function _download() {
    downloadURL=$(curl -s https://api.github.com/repos/ReGuilded/ReGuilded/releases/latest | grep browser_download_url | cut -d '"' -f 4)
    _clean
    echo "downloading latest reguilded.asar..."
    wget -q $downloadURL -O /tmp/reguilded.asar
}

function _install() {
    echo "installing ReGuilded..."
    echo -e "Closing Guilded (by force, ${KDELAY} second delay)..."
    echo "Press CTRL+C to cancel installation."
    sleep ${KDELAY}s
    killall -SIGKILL {g,G}uilded 2> /dev/null
    _download
    if [[ $1 == "flatpak" ]]; then
    		_flatpakPrep
        flatpakDir=$(flatpak info -l gg.guilded.Guilded)
        fullFlatpakDir=$(flatpak info -l gg.guilded.Guilded)/files/extra/guilded/resources
        mkdir -p $HOME/.reguilded
        cp /tmp/reguilded.asar $HOME/.reguilded/
        sudo flatpak override --filesystem=/home/$USER/.reguilded --system
        sudo mkdir -p $fullFlatpakDir/_guilded
        sudo mv $fullFlatpakDir/app.{asar,asar.unpacked} $fullFlatpakDir/_guilded/ 2> /dev/null
        sudo mkdir -p $fullFlatpakDir/app
        echo "require(\"/home/$USER/.reguilded/reguilded.asar\")" | sudo tee $fullFlatpakDir/app/index.js > /dev/null
        echo -e "{\n  \"name\": \"Guilded\",\n  \"main\": \"index.js\"\n}" | sudo tee $fullFlatpakDir/app/package.json > /dev/null
        echo ""
        echo -e "${green}installation is completed. You can now start Guilded and should be greeted by ReGuilded.${reset}"
        echo -e "${blue}Themes, Addons and Reguilded itself is stored in $HOME/.reguilded${reset}"
    elif [[ $1 == "system" ]]; then
        guildedDir="/opt/Guilded/resources"
        mkdir -p $HOME/.reguilded/
        cp /tmp/reguilded.asar $HOME/.reguilded/
        sudo mkdir -p $guildedDir/_guilded
        sudo mv $guildedDir/app.{asar,asar.unpacked} $guildedDir/_guilded 2> /dev/null
        sudo mkdir -p $guildedDir/app
        echo "require(\"/home/$USER/.reguilded/reguilded.asar\")" | sudo tee $guildedDir/app/index.js > /dev/null
        echo -e "{\n  \"name\": \"Guilded\",\n  \"main\": \"index.js\"\n}" | sudo tee $guildedDir/app/package.json > /dev/null
        echo ""
        echo -e "${green}installation is completed. You can now start Guilded and should be greeted by ReGuilded.${reset}"
        echo -e "${blue}Themes, Addons and Reguilded itself is stored in $HOME/.reguilded${reset}"
    else
        echo "other installation methods are not yet implemented."
        exit 1;
    fi
}

function _clean() {
    rm -rf $PWD/reguilded.asar* /tmp/reguilded.asar*
    echo "cleaned old reguilded.asar!"
}

function _help() {
    echo -e "
ReGuilded-CLI
Note that this CLI is unofficial, and not affiliated with the official ReGuilded dev-team.

    $ $0 [options] [parameters]
    Example: $0 --install flatpak

    Available Options:
    --install               To install ReGuilded. Valid Parameters: flatpak, system
    --clean                 Removes downloaded copies of reguilded.asar from the current directory.
    --help                  Shows the help page you're currently viewing.\n"
}

function _syntaxCheck() {
    if [[ -z $@ ]]; then
        echo "You have not provided any arguments! Make sure to run $0 --help if you're lost."
        exit 1;
    elif [[ ! -z $@ ]] && [[ $@="--install" ]]; then
        _install $2;
    elif [[ $@=* ]]; then
        echo "$@ is a set of invalid arguments!"
        echo "if you're lost, check $0 --help."
        exit 1;
    fi
}

case $@ in
    --help)
        _help;;
    --clean)
        _clean;;
    *)
        _syntaxCheck $1 $2;;
esac
