# ReGuilded-CLI: A Bash Commandline-Tool to install ReGuilded on Linux.

This is a small commandline tool which I've written to ease my installations of ReGuilded on Linux. My primary driving factor was the lack of Flatpak support in the official ReGuilded installer and the complexity of doing it manually for most users.

You can run this installer without downloading it by running the following command in the Terminal:

#### For Flatpak Installations (Check your software center if you're unsure of the source!)
```bash
bash -c "$(curl -fsSL https://codeberg.org/TGRush/ReGuilded-Cli/raw/branch/main/main.sh)" "" --install flatpak
```

#### For System-Installation / Official installer from guilded.gg
```bash
bash -c "$(curl -fsSL https://codeberg.org/TGRush/ReGuilded-Cli/raw/branch/main/main.sh)" "" --install system
```